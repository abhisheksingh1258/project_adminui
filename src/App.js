import React,{useState,useEffect} from 'react';
import axios from 'axios';
//import {link} from 'react-router-dom'

function App() {

  const [product,setProduct]=useState([]);
  const [search,setSearch]=useState("");
  
  const getProductData = async() =>{
    try{
      const data=await axios.get('https://geektrust.s3-ap-southeast-1.amazonaws.com/adminui-problem/members.json');
      console.log(data.data);
      setProduct(data.data);
    } catch(e){
      console.log(e);
    }
  };

  useEffect(()=>{
    getProductData();
  },[]);
  return (
    <div className="App">
      <input
      type="text"
      placeholder="search here"
      onChange={e=>{
        setSearch(e.target.value)
      }}>

      </input>
      {product
      .filter((item) => {
        if(search ===""){
          return item;
        }
        else if(item.name.toLowerCase().includes(search.toLowerCase())){
          return item;
        }
      })
      .map((item) =>{
        return (
        <p>
          {item.name},{item.email}
          </p>
          );
      
      })}
       
    </div>
  );
}

export default App;
